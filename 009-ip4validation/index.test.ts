import { isValidIP } from ".";

test("isValidIP('1.2.3.4') ➞ true", () =>
{
    const expected = true
    const actual = isValidIP("1.2.3.4")

    expect(actual).toBe(expected);
})

test("isValidIP('1.2.3') ➞ false", () =>
{
    const expected = false
    const actual = isValidIP("1.2.3")

    expect(actual).toBe(expected);
})

test("isValidIP('1.2.3.4.5') ➞ false", () =>
{
    const expected = false
    const actual = isValidIP("1.2.3.4.5")

    expect(actual).toBe(expected);
})


test("isValidIP('123.45.67.89') ➞ true", () =>
{
    const expected = true
    const actual = isValidIP("123.45.67.89")

    expect(actual).toBe(expected);
})

test("isValidIP('123.456.78.90') ➞ false", () =>
{
    const expected = false
    const actual = isValidIP("123.456.78.90")

    expect(actual).toBe(expected);
})

test("isValidIP('123.045.067.089') ➞ false", () =>
{
    const expected = false
    const actual = isValidIP("123.045.067.089")

    expect(actual).toBe(expected);
})