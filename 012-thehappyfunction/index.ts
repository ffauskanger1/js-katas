export function happyAlgorithm(number){
    return transform(number, [number], 0)
}

function transform (number, prev, count) {

    let result = 0
    let nums = Array.from(String(number), Number)
   
    for (let index = 0; index < nums.length; index++) {
        result += Math.pow(nums[index],2)
    }
    count++

    if(result === 1)
        return `HAPPY ${count}`
    if(prev.includes(result))
        return `SAD ${count}`    
    prev.push(result)
    return transform(result, prev, count)
}


