import { happyAlgorithm } from ".";

test("happyAlgorithm(139) ➞ 'HAPPY 5'", () =>
{
    const expected = "HAPPY 5"
    const actual = happyAlgorithm(139)

    expect(actual).toBe(expected);
})

test("happyAlgorithm(67) ➞ 'SAD 10'", () =>
{
    const expected = "SAD 10"
    const actual = happyAlgorithm(67)

    expect(actual).toBe(expected);
})



test("happyAlgorithm(1) ➞ 'HAPPY 1'", () =>
{
    const expected = "HAPPY 1"
    const actual = happyAlgorithm(1)

    expect(actual).toBe(expected);
})



test("happyAlgorithm(89) ➞ 'SAD 8'", () =>
{
    const expected = "SAD 8"
    const actual = happyAlgorithm(89)

    expect(actual).toBe(expected);
})

