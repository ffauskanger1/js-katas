import { countBoomerangs } from "."


test("countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞ 2", () =>
{
    const expected = 2
    const actual = countBoomerangs([9, 5, 9, 5, 1, 1, 1])

    expect(actual).toBe(expected);
})

test("countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞ 1", () =>
{
    const expected = 1
    const actual = countBoomerangs([5, 6, 6, 7, 6, 3, 9])

    expect(actual).toBe(expected);
})

test("countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞ 0", () =>
{
    const expected = 0
    const actual = countBoomerangs([4, 4, 4, 9, 9, 9, 9])

    expect(actual).toBe(expected);
})


test("countBoomerangs([1, 7, 1, 7, 1, 7, 1]) ➞ 5", () =>
{
    const expected = 5
    const actual = countBoomerangs([1, 7, 1, 7, 1, 7, 1])

    expect(actual).toBe(expected);
})

test("countBoomerangs([]) ➞ error", () =>
{
    const expected = "error"
    const actual = countBoomerangs([])

    expect(actual).toBe(expected);
})

test("countBoomerangs([1, 7]) ➞ error", () =>
{
    const expected = "error"
    const actual = countBoomerangs([1,7])

    expect(actual).toBe(expected);
})

test("countBoomerangs([1, 7, 1, 7, 'one', 7, 1]) ➞ error", () =>
{
    const expected = "error"
    const actual = countBoomerangs([1, 7, 1, 7, "one", 7, 1])

    expect(actual).toBe(expected);
})

test("countBoomerangs('test') ➞ error", () =>
{
    const expected = "error"
    const actual = countBoomerangs("test")

    expect(actual).toBe(expected);
})