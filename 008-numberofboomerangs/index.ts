export function countBoomerangs(arr)
{
    if(!Array.isArray(arr) || arr.length < 3 || arr.some(x => isNaN(x)))
        return "error"

    let counter = 0
    for (let index = 0; index < arr.length-2; index++) {
        if(arr[index] === arr[index+2] && arr[index] !== arr[index+1])
            counter++
    }
    return counter
}

