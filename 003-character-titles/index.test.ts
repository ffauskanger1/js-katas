import { correctTitle } from ".";

test("jOn SnoW, kINg IN thE noRth ➞ Jon Snow, King in the North.", () =>
{
    const expected = "Jon Snow, King in the North."
    const actual = correctTitle("jOn SnoW, kINg IN thE noRth")

    expect(actual).toBe(expected);
})

test("sansa stark,lady of winterfell. ➞ Sansa Stark, Lady of Winterfell.", () =>
{
    const expected = "Sansa Stark, Lady of Winterfell."
    const actual = correctTitle("sansa stark,lady of winterfell.")

    expect(actual).toBe(expected);
})


test("TYRION LANNISTER, HAND OF THE QUEEN. ➞ Tyrion Lannister, Hand of the Queen.", () =>
{
    const expected = "Tyrion Lannister, Hand of the Queen."
    const actual = correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")

    expect(actual).toBe(expected);
})
