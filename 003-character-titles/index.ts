export function correctTitle(title)
{

    let sentence = title.split(' ')
    let correctTitle = ""

    for (let index = 0; index < sentence.length; index++) {
        let word = sentence[index];
        
        if(word.includes(','))
        {
            let split = word.split(',');
            word = fixWord(split[0]) + ", " + fixWord(split[1])

            if(!word.endsWith(' '))
                word = word + " "

        }
        else
        {
            word = fixWord(word)
            if(!(word.endsWith('.')))
            {
                if(index === sentence.length - 1)
                    word = word + "."
                else
                    word = word + " "
            }
        }
        correctTitle += word
    }

    return correctTitle
}

function fixWord(word)
{
    let lowercaseWords = ["and","the","of","in"]
    let fixedWord = word.toLowerCase()
    
    if(!(lowercaseWords.some(e => e.includes(fixedWord))))
    {
        fixedWord = fixedWord.substring(0,1).toUpperCase() + fixedWord.substring(1)
    }
    return fixedWord;
}