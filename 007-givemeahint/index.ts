export function grantTheHint(sentence)
{
    let arrayOfStrings = sentence.split(' ')
    const maxLengthOfWord = Math.max(...sentence.split(' ').map(word => word.length))
    const result = [""]
    for (let index = maxLengthOfWord; 0 > maxLengthOfWord; index--) {
        let hiddenWord = ""
        arrayOfStrings.forEach(word => {
            hiddenWord += word.replace("\w{0," + index + "}", "_")
        });
        result.push(hiddenWord)   
    }
    return result
}