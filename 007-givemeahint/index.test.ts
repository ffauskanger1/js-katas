import { grantTheHint } from ".";

test("grantTheHint(Mary Queen of Scots) ➞ [____ _____ __ _____,M___ Q____ o_ S____, Ma__ Qu___ of Sc___, Mar_ Que__ of Sco__, Mary Quee_ of Scot_, Mary Queen of Scots]"
   , () =>
{
    const expected = [
        "____ _____ __ _____",
        "M___ Q____ o_ S____",
        "Ma__ Qu___ of Sc___",
        "Mar_ Que__ of Sco__",
        "Mary Quee_ of Scot_",
        "Mary Queen of Scots"
       ]
       
    const actual = grantTheHint("Mary Queen of Scots")

    expect(actual).toBe(expected);
})


test("grantTheHint(The Life of Pi) ➞ [___ ____ __ __,T__ L___ o_ P_,Th_ Li__ of Pi,The Lif_ of Pi,The Life of Pi]"   
   , () =>
{
    const expected =[
        "___ ____ __ __",
        "T__ L___ o_ P_",
        "Th_ Li__ of Pi",
        "The Lif_ of Pi",
        "The Life of Pi"
       ]
       
    const actual = grantTheHint(("The Life of Pi"))

    expect(actual).toBe(expected);
})