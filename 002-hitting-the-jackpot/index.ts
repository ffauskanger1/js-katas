export function Jackpot(outcome)
{
    if(!Array.isArray(outcome) || outcome.length !== 4)
        throw new Error("Not an array or not 4 elements")
    
    if(outcome.every(value => value === outcome[0]))
        return true
    
    return false
}