import { Jackpot } from ".";

test("([@, @, @, @]) ➞ true", () =>
{
    const expected = true
    const actual = Jackpot(["@", "@", "@", "@"])

    expect(actual).toBe(expected);
})


test("([abc, abc, abc, abc]", () =>
{
    const expected = true
    const actual = Jackpot(["abc", "abc", "abc", "abc"])

    expect(actual).toBe(expected);
})

test("([SS, SS, SS, SS]) ➞ true", () =>
{
    const expected = true
    const actual = Jackpot(["SS", "SS", "SS", "SS"])

    expect(actual).toBe(expected);
})

test("([&&, &, &&&, &&&&]) ➞ false", () =>
{
    const expected = false
    const actual = Jackpot(["&&", "&", "&&&", "&&&&"])

    expect(actual).toBe(expected);
})

test("([SS, SS, SS, Ss]) ➞ false", () =>
{
    const expected = false
    const actual = Jackpot(["SS", "SS", "SS", "Ss "])

    expect(actual).toBe(expected);
})