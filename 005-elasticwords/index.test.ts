import { elasticize } from ".";

test("elasticize(ANNA) ➞ ANNNNA", () =>
{
    const expected = "ANNNNA"
    const actual = elasticize("ANNA")

    expect(actual).toBe(expected);
})

test("elasticize(KAYAK) ➞ KAAYYYAAK", () =>
{
    const expected = "KAAYYYAAK"
    const actual = elasticize("KAYAK")

    expect(actual).toBe(expected);
})

test("elasticize(X) ➞ X", () =>
{
    const expected = "X"
    const actual = elasticize("X")

    expect(actual).toBe(expected);
})


test("elasticize(ABCD) ➞ ABBCCD", () =>
{
    const expected = "ABBCCD"
    const actual = elasticize("ABCD")

    expect(actual).toBe(expected);
})

test("elasticize(ABCDE) ➞ ABBCCCDDE", () =>
{
    const expected = "ABBCCCDDE"
    const actual = elasticize("ABCDE")

    expect(actual).toBe(expected);
})

test("elasticize(AB) ➞ AB", () =>
{
    const expected = "AB"
    const actual = elasticize("AB")

    expect(actual).toBe(expected);
})

