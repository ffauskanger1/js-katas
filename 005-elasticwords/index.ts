export function elasticize(word)
{
    return elasticizeHalf(word.substring(0,word.length/2)) + elasticizeHalf(word.substring(word.length/2).split("").reverse("").join("")).split("").reverse("").join("") 
}

function elasticizeHalf(word)
{
    let str = ""
    for (let index = 0; index < word.length; index++) {
        str += word[index].repeat(index + 1)
    }
    return str
}