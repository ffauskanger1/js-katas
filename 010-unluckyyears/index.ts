export const howUnlucky = year => isNaN(year) ? "error" : [0,1,2,3,4,5,6,7,8,9,10,11].filter(month => new Date(year,month,13).getDay() === 5).length
