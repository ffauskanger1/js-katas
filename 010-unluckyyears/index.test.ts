import { howUnlucky } from ".";

test("howUnlucky(2020) ➞ 2", () =>
{
    const expected = 2
    const actual = howUnlucky(2020)

    expect(actual).toBe(expected);
})

test("howUnlucky(2026) ➞ 3", () =>
{
    const expected = 3
    const actual = howUnlucky(2026)

    expect(actual).toBe(expected);
})

test("howUnlucky(2016) ➞ 1", () =>
{
    const expected = 1
    const actual = howUnlucky(2016)

    expect(actual).toBe(expected);
})
