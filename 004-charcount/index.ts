export function charCount(character, sentence) {
    if(character.length != 1)
        throw new Error("Character length should be 1")
    return [...sentence.matchAll(character)].length
}