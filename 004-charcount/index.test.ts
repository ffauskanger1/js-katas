import { charCount } from ".";

test("(a, edabit) ➞ 1", () =>
{
    const expected = 1
    const actual = charCount("a", "edabit")

    expect(actual).toBe(expected);
})

test("(c, Chamber of secrets) ➞ 1", () =>
{
    const expected = 1
    const actual = charCount("c", "Chamber of secrets")

    expect(actual).toBe(expected);
})

test("(B, boxes are fun) ➞ 0", () =>
{
    const expected = 0
    const actual = charCount("B", "boxes are fun")

    expect(actual).toBe(expected);
})

test("(b, big fat bubble) ➞ 4", () =>
{
    const expected = 4
    const actual = charCount("b", "big fat bubble")

    expect(actual).toBe(expected);
})

test("(e, javascript is good) ➞ 0", () =>
{
    const expected = 0
    const actual = charCount("e", "javascript is good")

    expect(actual).toBe(expected);
})

test("(!, !easy!) ➞ 2", () =>
{
    const expected = 2
    const actual = charCount("!", "!easy!")

    expect(actual).toBe(expected);
})

test("(wow, the universe is wow) ➞ error"
, () =>
{
    expect(() => {charCount("wow", "the universe is wow")}).toThrow("Character length should be 1")
})