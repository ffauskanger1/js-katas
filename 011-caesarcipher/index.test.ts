import { caesarCipher } from ".";

test("caesarCipher('middle-Outz', 2) ➞ 'okffng-Qwvb'", () =>
{
    const expected = "okffng-Qwvb"
    const actual = caesarCipher("middle-Outz", 2)

    expect(actual).toBe(expected);
})

test("caesarCipher('Always-Look-on-the-Bright-Side-of-Life', 5) ➞ 'Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj'", () =>
{
    const expected = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
    const actual = caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5)

    expect(actual).toBe(expected);
})


test("caesarCipher('A friend in need is a friend indeed', 20) ➞ 'U zlcyhx ch hyyx cm u zlcyhx chxyyx'", () =>
{
    const expected = "U zlcyhx ch hyyx cm u zlcyhx chxyyx"
    const actual = caesarCipher("A friend in need is a friend indeed", 20)

    expect(actual).toBe(expected);
})


test("number results in error", () =>
{
    const expected = "error"
    const actual = caesarCipher(12, 1)

    expect(actual).toBe(expected);
})

test("array results in error", () =>
{
    const expected = "error"
    const actual = caesarCipher(["t","f"], 1)

    expect(actual).toBe(expected);
})

test("empty string results in error", () =>
{
    const expected = "error"
    const actual = caesarCipher("", 1)

    expect(actual).toBe(expected);
})