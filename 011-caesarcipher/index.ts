export const caesarCipher = (toEncrypt, rotation) => isNaN(rotation) || !(typeof toEncrypt === 'string') || toEncrypt.length === 0 ? "error" : toEncrypt.split('').map(letter => shiftCharacter(letter, rotation)).join('')

function shiftCharacter(input, rotation)
{
    let character = input.toLowerCase().charCodeAt() - 97
    if(character < 0 || character > 26)
        return input
    let answer = (((character + rotation) % 26) + 97)
    
    return input == input.toUpperCase() ? String.fromCharCode(answer).toUpperCase() : String.fromCharCode(answer)
}