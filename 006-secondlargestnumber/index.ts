export function secondLargest(arr)
{
    if(!Array.isArray(arr))
        throw new Error("Not an array")
    if(arr.length == 0 || (arr.length == 2 && arr[0] == arr[1]))
        return 0
    if (arr.length == 1)
        return arr[0]

    // Sort array by numbers instead of strings
    return arr.sort((a, b) => (b - a))[1] // Second largest
}