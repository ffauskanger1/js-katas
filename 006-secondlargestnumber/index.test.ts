import { secondLargest } from ".";

test("secondLargest([10, 40, 30, 20, 50]) ➞ 40", () =>
{
    const expected = 40
    const actual = secondLargest([10, 40, 30, 20, 50])

    expect(actual).toBe(expected);
})

test("secondLargest([25, 143, 89, 13, 105]) ➞ 105", () =>
{
    const expected = 105
    const actual = secondLargest([25, 143, 89, 13, 105])

    expect(actual).toBe(expected);
})

test("secondLargest([54, 23, 11, 17, 10]) ➞ 23", () =>
{
    const expected = 23
    const actual = secondLargest([54, 23, 11, 17, 10])

    expect(actual).toBe(expected);
})

test("secondLargest([1, 1]) ➞ 0", () =>
{
    const expected = 0
    const actual = secondLargest([1, 1])

    expect(actual).toBe(expected);
})

test("secondLargest([1]) ➞ 1", () =>
{
    const expected = 1
    const actual = secondLargest([1])

    expect(actual).toBe(expected);
})

test("secondLargest([]) ➞ 0", () =>
{
    const expected = 0
    const actual = secondLargest([])

    expect(actual).toBe(expected);
})

