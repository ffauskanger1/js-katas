export function isPandigital(digits){
    let da = BigInt(digits).toString().split('')
    return ['0','1','2','3','4','5','6','7','8','9'].every(d => da.includes(d))
}