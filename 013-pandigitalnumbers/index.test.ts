import { isPandigital } from ".";

test("isPandigital(98140723568910) ➞ true", () =>
{
    const expected = true
    const actual = isPandigital(98140723568910)

    expect(actual).toBe(expected);
})

test("isPandigital(90864523148909) ➞ false", () =>
{
    const expected = false
    const actual = isPandigital(90864523148909)

    expect(actual).toBe(expected);
})

test("isPandigital(112233445566778899) ➞ false", () =>
{
    const expected = false
    const actual = isPandigital(112233445566778899)

    expect(actual).toBe(expected);
})